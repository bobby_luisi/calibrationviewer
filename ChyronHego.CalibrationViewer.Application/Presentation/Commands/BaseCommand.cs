﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Presentation.Commands
{
    public abstract class BaseCommand : ICommand
    {
        public ILogger Logger { get; set; }

        public IBusMessages MessageBus { get; set; }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter) => true;

        public async void Execute(object parameter)
        {
            await Task.Run(() =>
            {
                try
                {
                    ExecuteCommand(parameter);
                    throw new Exception();
                }
                catch (Exception exception)
                {
                    Logger.LogError(exception);
                }
            });
        }

        protected abstract void ExecuteCommand(object parameter);
    }
}