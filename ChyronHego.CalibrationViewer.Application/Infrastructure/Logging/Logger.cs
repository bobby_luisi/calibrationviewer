﻿using System;
using System.Diagnostics;
using ChyronHego.CalibrationViewer.Application.Events.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;
using log4net;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.Logging
{
    public class Logger : ILogger
    {
        private readonly ILog _logger;

        public Logger(string loggerName)
        {
            _logger = LogManager.GetLogger(loggerName);
            log4net.Config.XmlConfigurator.Configure();
        }

        public IBusMessages MessageBus { get; set; }

        private string CallerPrefix => new StackTrace().GetFrame(2).GetMethod().ToString();

        public void LogError(Exception exception) => _logger.Error($"{CallerPrefix} : {exception.Message}", exception);
        public void LogError(string message) => _logger.Error($"{CallerPrefix} : {message}");
        public void LogError(Exception exception, string message) => _logger.Error($"{CallerPrefix} : {message}", exception);
        public void LogWarning(string message) => _logger.Warn($"{CallerPrefix} : {message}");
        public void LogInfo(string message)
        {
            _logger.Info($"{message}");
            MessageBus?.Notify(new UpdateConsole
            {
                Message = message
            });
        }
    }
}
