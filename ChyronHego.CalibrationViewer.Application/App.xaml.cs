﻿using System.Windows;
using Autofac;
using ChyronHego.CalibrationViewer.Application.Infrastructure.DependencyInjection.Modules;
using ChyronHego.CalibrationViewer.Application.Presentation.ViewModels;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;

namespace ChyronHego.CalibrationViewer.Application
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        private static IContainer Container { get; set; }
        private ILogger _logger;

        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            InitializeContainer();

            _logger = Container.Resolve<ILogger>();

            _logger.LogInfo("Application started.");
        }

        private static void InitializeContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new InfrastructureLayerModule());
            builder.RegisterModule(new ApplicationLayerModule());
            builder.RegisterModule(new PresentationLayerModule());
            Container = builder.Build();

            var mainView = Container.Resolve<Presentation.Views.MainWindow>();
            mainView.DataContext = Container.Resolve<MainWindowViewModel>();
            mainView.Show();
        }

        private void App_OnExit(object sender, ExitEventArgs e)
        {
            _logger.LogInfo($"Application Exit.");
        }
    }
}
