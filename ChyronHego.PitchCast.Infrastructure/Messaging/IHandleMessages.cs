﻿namespace ChyronHego.CalibrationViewer.Infrastructure.Messaging
{
    public interface IHandleMessages<in T> where T : IMessage
    {
        void Handle(T message);
    }
}
