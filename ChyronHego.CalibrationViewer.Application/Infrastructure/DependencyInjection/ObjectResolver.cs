﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using ChyronHego.CalibrationViewer.Infrastructure.DependencyInjection;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.DependencyInjection
{
    public class ObjectResolver : IResolveObjects
    {
        private readonly IComponentContext _componentContext;

        public ObjectResolver(IComponentContext componentContext)
        {
            this._componentContext = componentContext;
        }

        public T Get<T>()
        {
            return _componentContext.Resolve<T>();
        }

        public T Get<T>(params Tuple<string, object>[] constructorArguments)
        {
            return this._componentContext.Resolve<T>(constructorArguments.Select(a => new NamedParameter(a.Item1, a.Item2)));
        }

        public IEnumerable<T> GetAll<T>()
        {
            return this._componentContext.Resolve<IEnumerable<T>>();
        }
    }
}
