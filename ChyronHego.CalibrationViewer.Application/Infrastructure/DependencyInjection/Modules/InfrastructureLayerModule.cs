﻿using Autofac;
using ChyronHego.CalibrationViewer.Application.Infrastructure.Logging;
using ChyronHego.CalibrationViewer.Application.Infrastructure.System.FileFetching;
using ChyronHego.CalibrationViewer.Infrastructure.DependencyInjection;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.DependencyInjection.Modules
{
    public class InfrastructureLayerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new Logger("CalibrationViewer")).As<ILogger>().SingleInstance().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.Register(c => new ObjectResolver(c.Resolve<IComponentContext>())).As<IResolveObjects>().SingleInstance();

            builder.RegisterType<MessageBus>().As<IBusMessages>().SingleInstance();

            builder.RegisterType<FileFetcher>().AsSelf().SingleInstance().PropertiesAutowired();
        }
    }
}
