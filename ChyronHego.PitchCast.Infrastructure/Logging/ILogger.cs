﻿using System;

namespace ChyronHego.CalibrationViewer.Infrastructure.Logging
{
    public interface ILogger
    {
        void LogError(Exception exception);

        void LogError(string message);

        void LogError(Exception exception, string message);

        void LogWarning(string message);

        void LogInfo(string message);
    }
}
