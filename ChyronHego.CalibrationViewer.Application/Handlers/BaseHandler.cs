﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Handlers
{
    public abstract class BaseHandler<T> : IHandleMessages<T> where T : IMessage
    {
        public ILogger Logger { get; set; }

        public IBusMessages MessageBus { get; set; }

        public void Handle(T message)
        {
            try
            {
                HandleMessage(message);
            }
            catch (Exception exception)
            {
                Logger.LogError(exception);
            }
        }

        protected abstract void HandleMessage(T message);
    }
}
