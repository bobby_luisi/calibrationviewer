﻿using Autofac;
using ChyronHego.CalibrationViewer.Application.Presentation.Commands.Configuration;
using ChyronHego.CalibrationViewer.Application.Services.Logging;
using ChyronHego.CalibrationViewer.Application.Services.Renderer;
using ChyronHego.CalibrationViewer.Application.Services.System;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.DependencyInjection.Modules
{
    public class ApplicationLayerModule : Module 
    {
        protected override void Load(ContainerBuilder builder)
        {
            
            // Services 
            builder.RegisterType<GS2RendererService>().AsSelf().SingleInstance().PropertiesAutowired();
            builder.RegisterType<LoadPmetFileService>().AsSelf().SingleInstance().PropertiesAutowired();
            builder.RegisterType<UpdateConsoleService>().AsSelf().SingleInstance().PropertiesAutowired();
        }
    }
}
