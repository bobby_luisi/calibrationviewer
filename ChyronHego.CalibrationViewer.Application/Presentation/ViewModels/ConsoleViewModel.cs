﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChyronHego.CalibrationViewer.Application.Presentation.ViewModels
{
    public class ConsoleViewModel : BaseViewModel
    {
        public string Log { get; set; } = string.Empty;

        public void RefreshConsoleText()
        {
            RaisePropertyChanged("Log");
        }
    }
}
