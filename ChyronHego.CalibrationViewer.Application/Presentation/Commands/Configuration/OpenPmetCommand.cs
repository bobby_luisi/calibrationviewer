﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChyronHego.CalibrationViewer.Application.Services.System;

namespace ChyronHego.CalibrationViewer.Application.Presentation.Commands.Configuration
{
    public class OpenPmetCommand : BaseCommand
    {
        public LoadPmetFileService LoadPmetFileService { get; set; }
        protected override void ExecuteCommand(object parameter)
        {
            LoadPmetFileService.Load();
        }
    }
}
