﻿using System.Windows;
using ChyronHego.CalibrationViewer.Infrastructure.DependencyInjection;

namespace ChyronHego.CalibrationViewer.Infrastructure.Messaging
{
    public class MessageBus : IBusMessages
    {
        private readonly IResolveObjects _objectResolver;

        public MessageBus(IResolveObjects objectResolver)
        {
            _objectResolver = objectResolver;
        }

        public void Notify<T>(T message) where T : IMessage
        {
            HandleMessage(_objectResolver, message);
        }

        private static void HandleMessage<T>(IResolveObjects objectResolver, T message) where T : IMessage
        {
            foreach (var handler in objectResolver.GetAll<IHandleMessages<T>>())
            {
                Application.Current.Dispatcher.Invoke(() => { handler.Handle(message); });
            }
        }
    }
}
