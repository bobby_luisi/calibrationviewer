﻿namespace ChyronHego.CalibrationViewer.Infrastructure.Messaging
{
    public interface IApplicationEvent : IMessage
    {
    }
}
