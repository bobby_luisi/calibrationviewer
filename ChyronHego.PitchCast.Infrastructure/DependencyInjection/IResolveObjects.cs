﻿using System;
using System.Collections.Generic;

namespace ChyronHego.CalibrationViewer.Infrastructure.DependencyInjection
{
    public interface IResolveObjects
    {
        T Get<T>();

        T Get<T>(params Tuple<string, object>[] constructorArguments);

        IEnumerable<T> GetAll<T>();
    }
}
