﻿namespace ChyronHego.CalibrationViewer.Infrastructure.Messaging
{
    public interface IBusMessages
    {
        void Notify<T>(T message) where T : IMessage;
    }
}
