﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChyronHego.CalibrationViewer.Application.Events.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Services.Logging
{
    public class UpdateConsoleService
    {
        public IBusMessages MessageBus { get; set; }

        public void Update(string mesasge)
        {
            MessageBus.Notify(new UpdateConsole
            {
                Message = mesasge
            });
        }
    }
}
