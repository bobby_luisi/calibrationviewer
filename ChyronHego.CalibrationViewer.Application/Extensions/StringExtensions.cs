﻿

namespace ChyronHego.CalibrationViewer.Application.Extensions
{
    public static class StringExtensions
    {
        public static int LineCount(this string str) => str.Split('\n').Length;
    }
}
