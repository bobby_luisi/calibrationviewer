﻿using Autofac;
using ChyronHego.CalibrationViewer.Application.Events.Logging;
using ChyronHego.CalibrationViewer.Application.Handlers.Logging;
using ChyronHego.CalibrationViewer.Application.Presentation.Commands.Configuration;
using ChyronHego.CalibrationViewer.Application.Presentation.Commands.TestCommands;
using ChyronHego.CalibrationViewer.Application.Presentation.ViewModels;
using ChyronHego.CalibrationViewer.Application.Presentation.Views;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.DependencyInjection.Modules
{
    public class PresentationLayerModule : Module 
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Views
            builder.RegisterType<MainWindow>().AsSelf().SingleInstance();

            // ViewModels
            builder.RegisterType<MainWindowViewModel>().AsSelf().SingleInstance().PropertiesAutowired();
            builder.RegisterType<ConsoleViewModel>().AsSelf().SingleInstance().PropertiesAutowired();

            // Commands 
            builder.RegisterType<TestCommand>().AsSelf().PropertiesAutowired();
            builder.RegisterType<OpenPmetCommand>().AsSelf().PropertiesAutowired();

            // Handlers
            builder.RegisterType<UpdateConsoleHandler>().As<IHandleMessages<UpdateConsole>>().PropertiesAutowired();
        }
    }
}
