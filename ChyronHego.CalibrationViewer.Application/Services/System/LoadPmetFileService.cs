﻿using System.Collections.Generic;
using ChyronHego.CalibrationViewer.Application.Infrastructure.System.FileFetching;
using ChyronHego.CalibrationViewer.Application.Services.Logging;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;

namespace ChyronHego.CalibrationViewer.Application.Services.System
{
    public class LoadPmetFileService : FileFetcher
    {
        public void Load()
        {
            LoadFile();
        }

        protected override void OnFileRead(ICollection<string> parsedLines)
        {
            // put XML data in model object. 
        }
    }
}
