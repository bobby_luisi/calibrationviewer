﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChyronHego.CalibrationViewer.Infrastructure.Messaging;

namespace ChyronHego.CalibrationViewer.Application.Events.Logging
{
    public class UpdateConsole : IApplicationEvent
    {
        public string Message { get; set; }
    }
}
