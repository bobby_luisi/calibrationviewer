﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChyronHego.CalibrationViewer.Infrastructure.Logging;
using Microsoft.Win32;

namespace ChyronHego.CalibrationViewer.Application.Infrastructure.System.FileFetching
{
    public abstract class FileFetcher
    {
        public ILogger Logger { get; set; }

        public void LoadFile()
        {
            Task.Run(() =>
            {
                try
                {
                    var openFileDialog = new OpenFileDialog();
                    if (openFileDialog.ShowDialog() == false) return;
                    var fileLinesRead = File.ReadAllLines(openFileDialog.FileName).ToList();
                    OnFileRead(fileLinesRead);
                }
                catch (Exception e)
                {
                    Logger.LogError(e);
                }
            });
        }

        protected abstract void OnFileRead(ICollection<string> parsedLines);
    }
}
