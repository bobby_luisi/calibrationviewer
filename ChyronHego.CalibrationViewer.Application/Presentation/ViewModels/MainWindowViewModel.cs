﻿

using ChyronHego.CalibrationViewer.Application.Presentation.Commands.Configuration;
using ChyronHego.CalibrationViewer.Application.Presentation.Commands.TestCommands;
using ChyronHego.CalibrationViewer.Application.Services.System;

namespace ChyronHego.CalibrationViewer.Application.Presentation.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        // View Models
        public ConsoleViewModel ConsoleViewModel { get; set; }
        
        // Commands
        public TestCommand TestCommand { get; set; }
        public OpenPmetCommand OpenPmetCommand { get; set; }

        public string ButtonLabel { get; set; } = "Test Button";
    }
}
