﻿using System;
using ChyronHego.CalibrationViewer.Application.Events.Logging;
using ChyronHego.CalibrationViewer.Application.Extensions;
using ChyronHego.CalibrationViewer.Application.Presentation.ViewModels;

namespace ChyronHego.CalibrationViewer.Application.Handlers.Logging
{
    public class UpdateConsoleHandler : BaseHandler<UpdateConsole>
    {
        private const int MaxLineCount = 25;
        public ConsoleViewModel ConsoleViewModel { get; set; }
        protected override void HandleMessage(UpdateConsole message)
        {
            var logLineCount = ConsoleViewModel.Log.LineCount();

            if (logLineCount >= MaxLineCount)
            {
                ConsoleViewModel.Log = ConsoleViewModel.Log.Remove(ConsoleViewModel.Log.LastIndexOf('\n'));
                ConsoleViewModel.RefreshConsoleText();
            }

            if (string.IsNullOrEmpty(ConsoleViewModel.Log))
            {
                ConsoleViewModel.Log = $"{DateTime.Now.ToLongTimeString()}: {message.Message}";
                ConsoleViewModel.RefreshConsoleText();
            }
            else
            {
                ConsoleViewModel.Log = $"{DateTime.Now.ToLongTimeString()}: {message.Message}\n{ConsoleViewModel.Log}";
                ConsoleViewModel.RefreshConsoleText();
            }
        }
    }
}
