﻿namespace ChyronHego.CalibrationViewer.Infrastructure.Apache
{
    public interface IAmqTopicConsumer
    {
        string Uri { get; set; }

        string TopicName { get; set; }

        string Username { get; set; }

        string Password { get; set; }

        string ClientId { get; set; }

        void Start();

        void Stop();
    }
}
