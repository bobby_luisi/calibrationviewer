﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ChyronHego.CalibrationViewer.Infrastructure.Process
{
    public class ProcessManager
    {
        private System.Diagnostics.Process _process;
        private bool _isRunning;

        public ProcessManager()
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    await Task.Delay(1000); 

                    if (string.IsNullOrEmpty(ProcessName))
                    {
                        continue;
                    }

                    try
                    {
                        var processes = System.Diagnostics.Process.GetProcessesByName(ProcessName);
                        IsRunning = processes.Length > 0;
                    }
                    catch
                    {
                        // suppressed
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        public string ProcessName { get; set; }

        public string ApplicationPath { get; set; }

        public bool IsRunning
        {
            get { return _isRunning; }
            set
            {
                if (_isRunning != value)
                {
                    _isRunning = value;
                    RunningChanged?.Invoke(value);
                }
            }
        }

        public event Action Started;
        public event Action<bool> RunningChanged;

        public void Start()
        {
            Kill();
            Initialize();
        }

        private void Initialize()
        {
            var processes = System.Diagnostics.Process.GetProcessesByName(ProcessName);

            if (processes.Length == 0)
            {
                var processInfo = new ProcessStartInfo(ApplicationPath);

                _process = System.Diagnostics.Process.Start(processInfo);

                if (_process == null)
                {
                    return;
                }

                var started = false;

                while (!_process.HasExited && !started)
                {
                    try
                    {
                        started = _process.WaitForInputIdle();
                    }
                    catch
                    {
                        break;
                    }
                }

                Started?.Invoke();
            }
            else
            {
                _process = processes[0];
            }
        }

        private void Kill()
        {
            var processes = System.Diagnostics.Process.GetProcessesByName(ProcessName);

            if (processes.Length > 0)
            {
                processes[0].Kill();
                processes[0].WaitForExit(1000);
            }
        }
    }
}
